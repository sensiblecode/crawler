1. invoke crawler with:

* `scrapy crawl longlegs`
* `-a urls`: start urls
* `-a domains`: permitted domains
* `-a deny`: URLs must not match any regex to be crawled
* `-a allow`: URLs not matching any regex are not crawled
* `-o`: followed by name of output file
* `-t jsonlines`

All `-a` parameters are JSON list-of-strings.


2. Generates a database containing:

request url
response url
content body
content type header (raw)
headers (as a raw line separated list)
parent url

Use cases:

* raw data to feed through Goose to generate NAF files for articles on websites
* getting an idea of surveying URLs for a domain e.g. "how many PDFs?"
* search in a site for keywords
