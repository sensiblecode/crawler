import scrapy.dupefilter
import collections
import urlparse
import cgi
import urllib
import scrapy.conf
import logging
import scrapy.utils.url
from w3lib.url import safe_url_string


def canonicalize_url(url, keep_blank_values=True, keep_fragments=False,
        encoding=None, keep_params=True, keep_queries=True):
    """as scrapy/utils/url.py, but allows for optional purging paths and queries"""

    """Canonicalize the given url by applying the following procedures:

    - sort query arguments, first by key, then by value
    - percent encode paths and query arguments. non-ASCII characters are
      percent-encoded using UTF-8 (RFC-3986)
    - normalize all spaces (in query arguments) '+' (plus symbol)
    - normalize percent encodings case (%2f -> %2F)
    - remove query arguments with blank values (unless keep_blank_values is True)
    - remove fragments (unless keep_fragments is True)

    The url passed can be a str or unicode, while the url returned is always a
    str.

    For examples see the tests in scrapy.tests.test_utils_url
    """

    scheme, netloc, path, params, query, fragment = scrapy.utils.url.parse_url(url)
    keyvals = cgi.parse_qsl(query, keep_blank_values)
    keyvals.sort()
    query = urllib.urlencode(keyvals)
    path = safe_url_string(_unquotepath(path)) or '/'
    fragment = '' if not keep_fragments else fragment
    query = '' if not keep_queries else query  # new
    params = '' if not keep_params else params
    return urlparse.urlunparse((scheme, netloc.lower(), path, params, query, fragment))


def _unquotepath(path):
    for reserved in ('2f', '2F', '3f', '3F'):
        path = path.replace('%' + reserved, '%25' + reserved.upper())
    return urllib.unquote(path)


def has_query_or_path(url):
    """things which return true here must have the corresponding option in
    url_fingerprint as false."""
    scheme, netloc, path, params, query, fragment = scrapy.utils.url.parse_url(url)
    return bool(query or params)


def url_fingerprint(url):
    return canonicalize_url(url, keep_params=False, keep_queries=False)


def remove_query(url):
    """dump the params, query and fragment strings
    >>> remove_query("http://dave:passwd@myurl.com:80/path;123?123#123")
    'http://dave:passwd@myurl.com:80/path'
    """
    # params = ";sessionID..."
    # query = "?key=SECRET"
    # fragment = "#MyHREF"
    parse = list(urlparse.urlparse(url))
    newurl = [parse[0], parse[1], parse[2], '', '', '']
    return urlparse.urlunparse(newurl)


class ManyQueryDupeFilter(scrapy.dupefilter.RFPDupeFilter):
    def __init__(self, path=None):
        self.MAX = scrapy.conf.settings.getint('MANYQUERY_MAX')
        # TODO understand if need to handle file
        self.query_fingerprints = collections.Counter()
        super(ManyQueryDupeFilter, self).__init__(path)

    def request_seen(self, request):
        if has_query_or_path(request.url):
            count = self.query_fingerprints[url_fingerprint(request.url)]
            self.query_fingerprints[url_fingerprint(request.url)] += 1
            if count == self.MAX:
                logging.warn("ManyQueryDupeFilter: seen {url} {count} times".format(url=request.url, count=count))
            if count >= self.MAX:
                return True
        return super(ManyQueryDupeFilter, self).request_seen(request)
