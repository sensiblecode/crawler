# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

class SilkwormItem(Item):
    # define the fields for your item here like:
    # name = Field()
    request_url = Field()
    response_url = Field()
    parent_url = Field()
    content = Field()
    mime_type = Field()
    character_set = Field()
