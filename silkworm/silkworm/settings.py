# Scrapy settings for silkworm project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

import os

BOT_NAME = 'silkworm'

SPIDER_MODULES = ['silkworm.spiders']
NEWSPIDER_MODULE = 'silkworm.spiders'

USER_AGENT = 'silkworm (+http://www.scraperwiki.com)'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.httpcache.HttpCacheMiddleware': 101,
    'scrapy.contrib.downloadermiddleware.robotstxt.RobotsTxtMiddleware': 99
}

# Cache EVERYTHING...
HTTPCACHE_POLICY = 'scrapy.contrib.httpcache.DummyPolicy'
# ... in a database ...
HTTPCACHE_STORAGE = 'scrapy.contrib.httpcache.DbmCacheStorage'
# ... no, really.
HTTPCACHE_ENABLED = True

ROBOTSTXT_OBEY = True

# TL;DR - Fifo = Breadth first.
# http://doc.scrapy.org/en/latest/faq.html#does-scrapy-crawl-
#     in-breadth-first-or-depth-first-order
DEPTH_PRIORITY = 1
SCHEDULER_DISK_QUEUE = 'scrapy.squeue.PickleFifoDiskQueue'
SCHEDULER_MEMORY_QUEUE = 'scrapy.squeue.FifoMemoryQueue'
DEPTH_LIMIT = 10

DOWNLOAD_DELAY = 2

DUPEFILTER_CLASS = 'silkworm.dupefilters.ManyQueryDupeFilter'
MANYQUERY_MAX = 10

# LOG_LEVEL = "INFO"
# LOG_FILE = os.path.join(os.getenv("HOME"), "log", "scrapy.log")
