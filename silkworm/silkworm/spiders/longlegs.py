import re
import json
import os
import logging
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.spider import BaseSpider
from silkworm.items import SilkwormItem
from scrapy.http import Request, HtmlResponse

log_path = os.path.join(os.getenv("HOME"), "log")
log_file = os.path.join(log_path, 'scrapy_url_log.log')

try:
    os.mkdir(log_path)
except OSError as e:
    logging.info("Couldn't make directory: probably already exists: {!r}".format(e))

logging.basicConfig(filename=log_file,
                    level=logging.INFO,
                    format='%(message)s')
url_log = logging.getLogger(__name__)


def extract_mime_type(content_type):
    """
    >>> extract_mime_type('text/html; charset=utf-8')
    'text/html'
    >>> extract_mime_type('application/xml')
    'application/xml'
    >>> extract_mime_type('application/json;charset=latin-1;jam=tasty')
    'application/json'
    >>>
    """
    return content_type.partition(";")[0].strip()


def extract_character_set(content_type):
    """
    >>> extract_character_set('text/html; charset=utf-8')
    'utf-8'
    >>> extract_character_set('application/xml')
    >>> extract_character_set('application/json;charset=latin-1;jam=tasty')
    'latin-1'
    """
    charset = re.search(r';\s*charset=([^;]*);?', content_type)
    if charset:
        return charset.group(1)
    else:
        return None


class LonglegsSpider(BaseSpider):
    name = 'longlegs'
    # TODO deny_extensions=['xls', 'xlsx'] is an option to SgmlLinkExtractor

    def __init__(self, urls, domains, allow='[]', deny='[]', *args, **kwargs):
        print deny
        self.allowed_domains = json.loads(domains)
        self.start_urls = json.loads(urls)
        self.allow_regexes = json.loads(allow)
        self.deny_regexes = json.loads(deny)
        self.extractor = SgmlLinkExtractor(allow=self.allow_regexes,
                                            deny=self.deny_regexes)
        super(LonglegsSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        for request in self.request_new_links(response):
            yield request
        for item in self.parse_item(response):
            yield item

    def request_new_links(self, response):
        """ heavily edited from https://github.com/scrapy/scrapy/blob/
            master/scrapy/contrib/spiders/crawl.py """
        if not isinstance(response, HtmlResponse):
            return
        links = [l for l in self.extractor.extract_links(response)]
        for link in links:
            r = Request(url=link.url, callback=self.parse)
            r.meta.update(link_text=link.text,
                          parent_url=response.url,
                          request_url=link.url)
            yield r
        url_log.info("Parsed {}".format(response.url))

    def parse_item(self, response):
        # hxs = HtmlXPathSelector(response)
	if response.request.url != response.url:
	    url_log.warn("{} != {}".format(response.request.url, response.url))
        # assert response.request.url == response.url  # can't remember why this is here!
        i = SilkwormItem()
        i['request_url'] = response.meta.get('request_url', None)
        i['response_url'] = response.url
        i['parent_url'] = response.meta.get('parent_url', None)
        i['content'] = response.body
        i['mime_type'] = extract_mime_type(response.headers['Content-Type'])
        i['character_set'] = extract_character_set(response.headers['Content-Type'])
        #i['domain_id'] = hxs.select('//input[@id="sid"]/@value').extract()
        #i['name'] = hxs.select('//div[@id="name"]').extract()
        #i['description'] = hxs.select('//div[@id="description"]').extract()
        yield i
