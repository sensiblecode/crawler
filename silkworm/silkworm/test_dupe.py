from nose.tools import assert_equal
from nose import SkipTest
from dupefilters import ManyQueryDupeFilter
import scrapy.settings

def run_through_urls(urllist):
    dupe = ManyQueryDupeFilter()
    dupe.MAX = 2
    for i, (url, expect_pass) in enumerate(urllist):
        answer = dupe.request_seen(MockRequest(url))
        assert bool(answer) == expect_pass, \
            "{}, {} returned {}, not {}".format(i, url, answer, expect_pass)


class MockRequest(object):
    def __init__(self, url):
        self.url = url

    def __getattr__(self, attribute):
        return attribute


# EXTRACT LINKS
def test_works_fine_normally():
    urllist = [
        ["http://www.google.com", False],
        ["http://www.google.com/nope", False],
        ["http://www.google.com/ig", False]
    ]
    run_through_urls(urllist)


def test_stops_after_some_queries():
    urllist = [
        ["http://www.example.com?aaa", False],
        ["http://www.example.com?aab", False],
        ["http://www.example.com?aac", True],
    ]
    run_through_urls(urllist)

def test_stops_after_some_params():
    urllist = [
        ["http://www.example.com/;xyz", False],
        ["http://www.example.com/;abc", False],
        ["http://www.example.com/;nope", True],
    ]
    run_through_urls(urllist)

def test_stops_after_combo():
    urllist = [
        ["http://www.example.com;aaa", False],
        ["http://www.example.com?zzz", False],
        ["http://www.example.com/bbb;ccc?ddd", True]
    ]
