from nose.tools import assert_equal
from nose import SkipTest


# EXTRACT LINKS
def test_extracts_valid_links():
    raise SkipTest("Implemented by Scrapy")


def test_doesnt_extract_invalid_links():
    raise SkipTest("Implemented by Scrapy")


def test_follows_meta_refresh_redirects():
    raise SkipTest("Implemented by Scrapy")


def test_links_are_absolute():
    raise SkipTest("Implemented by Scrapy")


def test_uses_redirected_url_for_absolute_urls():
    raise SkipTest("Implemented by Scrapy. Probably.")


# FILTER URLS
def test_is_restricted_to_permitted_domains():
    raise SkipTest("Implemented by Scrapy")


def test_respects_robots_txt():
    raise SkipTest("Implemented by Scrapy")


def test_detects_infinite_unique_urls():
    raise SkipTest("see separate test")


def test_ignores_by_mimetype():
    raise SkipTest("Not core.")


def test_ignores_by_extension():
    raise SkipTest("Not core.")


# WISHFUL THINKING
def test_records_semantic_path():
    raise SkipTest("Can reconstruct from parent, later.")


def test_handles_javascript():
    raise SkipTest("Not happening any time soon.")


def test_supports_post():
    raise SkipTest("Not core functionality")


# FAILURE MODES
def test_fails_to_decode_gracefully():
    raise SkipTest


def test_decodes_unusual_encodings_successfully():
    raise SkipTest


def test_doesnt_explode_on_500_error():
    raise SkipTest


def test_doesnt_explode_on_timeout_error():
    raise SkipTest
